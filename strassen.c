#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int **alocaMemoria (int tamanho); /* Retorna uma matriz quadrada bi-dimensional implementada por ponteiros */

void liberaMemoria (int **matriz, int tamanho); /* Libera toda a memória alocada pela função alocaMemoria */

int inteiroAleatorio(int chao, int teto); /* Retorna um número inteiro aleatório entre chao e teto, inclusivo */

void inicializaMatriz (int **matriz, int tamanho); /* Atribui valores para os elementos da matriz utilizando a função inteiroAleatorio*/

int **multiplicaNormal (int **matrizA, int **matrizB, int tamanho); /* Implementação do algoritmo de multiplicação tradicional*/

void divideBlocos (int **matriz, int **blocoA, int **blocoB, int **blocoC, int **blocoD, int tamanho); /* Atribui os valores da matriz em quatro blocos de mesmo tamanho */

int  **reatribuiBlocos (int **blocoA, int **blocoB, int **blocoC, int **blocoD, int tamanho); /* Reintegra os blocos em uma única matriz */

int **soma(int **matrizA, int **matrizB, int tamanho); /* Implementação de soma de matrizes */

int **subtracao(int **matrizA, int **matrizB, int tamanho); /* Implementação de subtração de matrizes */

int **multiplicaStrassen (int **matrizA, int **matrizB, int tamanho, int hibrido); /* Implementação do algoritmo de Strassen, podendo também utilizar o algoritmo tradicional para tamanhos menores que 1024 */

void teste (int **matrizProdutoNormal, int **matrizProdutoStrassen, int tamanho); /* Testa se as matrizes obtidas por implementações diferentes são iguais*/

void imprimeMatriz (int **matriz, int tamanho); /* Imprime todos os elementos da matriz */

int main (int argc, char *argv[]) {

	double tempo;
	clock_t inicio, fim;

    int tamanho;

    int **matrizA, **matrizB, **matrizProdutoNormal, **matrizProdutoStrassen, **matrizProdutoHibrido;

    if (argc != 3) {
			printf("Número de argumentos de linha de comando diferente do esperado.\n");
			return -1;
	}
	if (strcmp(argv[1],"-n")) {
			printf("Argumento de linha de comando diferente do esperado.\n");
			return -1;
	}

    tamanho = atoi(argv[2]);

    matrizA = alocaMemoria(tamanho);
    matrizB = alocaMemoria(tamanho);

    srand(time(NULL));
    
    inicializaMatriz(matrizA, tamanho);
    inicializaMatriz(matrizB, tamanho);

    /*printf("Matriz A : \n");
    imprimeMatriz(matrizA, tamanho);
    printf("Matriz B : \n");
    imprimeMatriz(matrizB, tamanho);*/
    
    inicio = clock();
    matrizProdutoNormal = multiplicaNormal(matrizA, matrizB, tamanho);
    fim = clock();
    tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    /*printf("Matriz A x B  por multiplicação normal: \n");
    imprimeMatriz(matrizProdutoNormal, tamanho);*/
    printf("O tempo de execução pela multiplicação normal é de %f segundos\n\n", tempo);
    
    inicio = clock();
    matrizProdutoStrassen = multiplicaStrassen(matrizA, matrizB, tamanho, 0);
    fim = clock();
    tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    /*printf("Matriz A x B por multiplicação de Strassen: \n");
    imprimeMatriz(matrizProdutoStrassen, tamanho);*/
    printf("O tempo de execução pela multiplicação por Strassen é de %f segundos\n\n", tempo);

    inicio = clock();
    matrizProdutoHibrido = multiplicaStrassen(matrizA, matrizB, tamanho, 1);
    fim = clock();
    tempo = (double)(fim - inicio) / CLOCKS_PER_SEC;
    /*printf("Matriz A x B por multiplicação híbrida: \n");
    imprimeMatriz(matrizProdutoHibrida, tamanho);*/
    printf("O tempo de execução pela multiplicação híbrida é de %f segundos\n\n", tempo);

    liberaMemoria(matrizA, tamanho);
    liberaMemoria(matrizB, tamanho);
    liberaMemoria(matrizProdutoNormal, tamanho);
    liberaMemoria(matrizProdutoStrassen, tamanho);
    liberaMemoria(matrizProdutoHibrido, tamanho);
    
    return 0;
}

int **alocaMemoria (int tamanho) { 

    int i;

    int **matriz = malloc(sizeof(int*) * tamanho);

    for (i = 0; i < tamanho; i++) {
        matriz[i] = malloc(sizeof(int) * tamanho);
    }

    return matriz;
}

void liberaMemoria (int **matriz, int tamanho) {

    int i;

    for (i = 0; i < tamanho; i++) {
        free(matriz[i]);
    }

    free(matriz);
}

int inteiroAleatorio(int chao, int teto) {

    int aleatorio, divisor = RAND_MAX / (teto + 1);

    do {
        aleatorio = rand() / divisor;
    }   while (aleatorio < chao || aleatorio > teto);

    return aleatorio;
}

void inicializaMatriz (int **matriz, int tamanho) {

    int i, j;

    for (i = 0; i < tamanho; i++) {
        for (j = 0; j < tamanho; j++) {
            matriz[i][j] = inteiroAleatorio(0, 10);
        }
    }
}

int **multiplicaNormal (int **matrizA, int **matrizB, int tamanho) {

    int m, n, i;

    int **matrizProduto = alocaMemoria(tamanho);

    for (m = 0; m < tamanho; m++) {
            for (n = 0; n < tamanho; n++) {
                matrizProduto[m][n] = 0;
                for (i = 0; i < tamanho; i++) {
                    matrizProduto[m][n] += matrizA[m][i] * matrizB[i][n];
                }
            }
    }

    return matrizProduto;
}

void divideBlocos (int **matriz, int **blocoA, int **blocoB, int **blocoC, int **blocoD, int tamanho) {

    int m, n, i, j;

    for (i = 0, m = 0; i < tamanho/2; i++, m++) {
        for (j = 0, n = 0; j < tamanho/2; j++, n++) {
            blocoA[i][j] = matriz[m][n];
        }
    }

    for (i = 0, m = 0; i < tamanho/2; i++, m++) {
        for (j = 0, n = tamanho/2; j < tamanho/2; j++, n++) {
            blocoB[i][j] = matriz[m][n];
        }
    }

    for (i = 0, m = tamanho/2; i < tamanho/2; i++, m++) {
        for (j = 0, n = 0; j < tamanho/2; j++, n++) {
            blocoC[i][j] = matriz[m][n];
        }
    }

    for (i = 0, m = tamanho/2; i < tamanho/2; i++, m++) {
        for (j = 0, n = tamanho/2; j < tamanho/2; j++, n++) {
            blocoD[i][j] = matriz[m][n];
        }
    }
}

int  **reatribuiBlocos (int **blocoA, int **blocoB, int **blocoC, int **blocoD, int tamanho) {

    int **matriz = alocaMemoria(tamanho);
    
    int m, n, i, j;

    for (i = 0, m = 0; i < tamanho/2; i++, m++) {
        for (j = 0, n = 0; j < tamanho/2; j++, n++) {
            matriz[m][n] = blocoA[i][j];
        }
    }

    for (i = 0, m = 0; i < tamanho/2; i++, m++) {
        for (j = 0, n = tamanho/2; j < tamanho/2; j++, n++) {
            matriz[m][n] = blocoB[i][j];
        }
    }

    for (i = 0, m = tamanho/2; i < tamanho/2; i++, m++) {
        for (j = 0, n = 0; j < tamanho/2; j++, n++) {
            matriz[m][n] = blocoC[i][j];
        }
    }

    for (i = 0, m = tamanho/2; i < tamanho/2; i++, m++) {
        for (j = 0, n = tamanho/2; j < tamanho/2; j++, n++) {
            matriz[m][n] = blocoD[i][j];
        }
    }

    return matriz;   
}

int **soma(int **matrizA, int **matrizB, int tamanho) {

    int i, j;

    int **matriz = alocaMemoria(tamanho);

    for (i = 0; i < tamanho; i++) {
        for (j = 0; j < tamanho; j++) {
            matriz[i][j] = matrizA[i][j] + matrizB[i][j];
        }
    }

    return matriz;
}

int **subtracao(int **matrizA, int **matrizB, int tamanho) {

    int i, j;

    int **matriz = alocaMemoria(tamanho);

    for (i = 0; i < tamanho; i++) {
        for (j = 0; j < tamanho; j++) {
            matriz[i][j] = matrizA[i][j] - matrizB[i][j];
        }
    }

    return matriz;
}

int **multiplicaStrassen (int **matrizA, int **matrizB, int tamanho, int hibrido) {

    int limite;
    int **matrizProduto;
    int **blocoA, **blocoB, **blocoC, **blocoD, **blocoE, **blocoF, **blocoG, **blocoH;
    int **somaA, **somaB, **somaC, **somaD;
    int **bloco1, **bloco2, **bloco3, **bloco4, **bloco5, **bloco6, **bloco7;

    switch (hibrido) {

        case 0 :
            limite = 1;
            break;
        case 1 :
            limite = 1024;
            break;
    }

    if (tamanho <= limite) {

        matrizProduto = alocaMemoria(tamanho);

        matrizProduto = multiplicaNormal(matrizA, matrizB, tamanho);

        return matrizProduto;

    }
    else {
        
        matrizProduto = alocaMemoria(tamanho); 

        blocoA = alocaMemoria(tamanho/2);
        blocoB = alocaMemoria(tamanho/2);
        blocoC = alocaMemoria(tamanho/2);
        blocoD = alocaMemoria(tamanho/2);
        blocoE = alocaMemoria(tamanho/2);
        blocoF = alocaMemoria(tamanho/2);
        blocoG = alocaMemoria(tamanho/2);
        blocoH = alocaMemoria(tamanho/2);

        somaA = alocaMemoria(tamanho/2);
        somaB = alocaMemoria(tamanho/2);
        somaC = alocaMemoria(tamanho/2);
        somaD = alocaMemoria(tamanho/2);

        bloco1 = alocaMemoria(tamanho/2);
        bloco2 = alocaMemoria(tamanho/2);
        bloco3 = alocaMemoria(tamanho/2);
        bloco4 = alocaMemoria(tamanho/2);
        bloco5 = alocaMemoria(tamanho/2);
        bloco6 = alocaMemoria(tamanho/2);
        bloco7 = alocaMemoria(tamanho/2);
        
        divideBlocos(matrizA, blocoA, blocoB, blocoC, blocoD, tamanho);
        divideBlocos(matrizB, blocoE, blocoF, blocoG, blocoH, tamanho);

        bloco1 = multiplicaStrassen(soma(blocoA, blocoD, tamanho/2), soma(blocoE, blocoH, tamanho/2), tamanho/2, hibrido);
        bloco2 = multiplicaStrassen(blocoD, subtracao(blocoG, blocoE, tamanho/2), tamanho/2, hibrido);
        bloco3 = multiplicaStrassen(soma(blocoA, blocoB, tamanho/2), blocoH, tamanho/2, hibrido);
        bloco4 = multiplicaStrassen(subtracao(blocoB, blocoD, tamanho/2), soma(blocoG, blocoH, tamanho/2), tamanho/2, hibrido);
        bloco5 = multiplicaStrassen(blocoA, subtracao(blocoF, blocoH, tamanho/2), tamanho/2, hibrido);
        bloco6 = multiplicaStrassen(soma(blocoC, blocoD, tamanho/2), blocoE, tamanho/2, hibrido);
        bloco7 = multiplicaStrassen(subtracao(blocoA, blocoC, tamanho/2), soma(blocoE, blocoF, tamanho/2), tamanho/2, hibrido);

        somaA = soma(subtracao(soma(bloco1, bloco2, tamanho/2), bloco3, tamanho/2), bloco4, tamanho/2);
        somaB = soma(bloco5, bloco3, tamanho/2);
        somaC = soma(bloco6, bloco2, tamanho/2);
        somaD = subtracao(subtracao(soma(bloco5, bloco1, tamanho/2), bloco6, tamanho/2), bloco7, tamanho/2);

        matrizProduto = reatribuiBlocos(somaA, somaB, somaC, somaD, tamanho);

        liberaMemoria(blocoA, tamanho/2);
        liberaMemoria(blocoB, tamanho/2);
        liberaMemoria(blocoC, tamanho/2);
        liberaMemoria(blocoD, tamanho/2);
        liberaMemoria(blocoE, tamanho/2);
        liberaMemoria(blocoF, tamanho/2);
        liberaMemoria(blocoG, tamanho/2);
        liberaMemoria(blocoH, tamanho/2);
        
        liberaMemoria(somaA, tamanho/2);
        liberaMemoria(somaB, tamanho/2);
        liberaMemoria(somaC, tamanho/2);
        liberaMemoria(somaD, tamanho/2);

        liberaMemoria(bloco1, tamanho/2);
        liberaMemoria(bloco2, tamanho/2);
        liberaMemoria(bloco3, tamanho/2);
        liberaMemoria(bloco4, tamanho/2);
        liberaMemoria(bloco5, tamanho/2);
        liberaMemoria(bloco6, tamanho/2);
        liberaMemoria(bloco7, tamanho/2);

        return matrizProduto;
    } 
}

void teste (int **matrizProdutoNormal, int **matrizProdutoStrassen, int tamanho) {
    
    int i, j, erro = 0;
    
    for (i = 0; i < tamanho; i++) {
        for (j = 0; j < tamanho; j++) {
            if (matrizProdutoNormal[i][j] != matrizProdutoStrassen[i][j]){
                erro += 1;
            }
        }   
    }

    if (erro == 0) {
        printf("As matrizes são iguais!\n");
    }
    else {
        printf("As matrizes são diferentes!\n");
    }
}

void imprimeMatriz (int **matriz, int tamanho) {

    int i, j;

    for (i = 0; i < tamanho; i++) {
        for (j = 0; j < tamanho; j++) {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}